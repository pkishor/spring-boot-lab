package com.corp.abc.banking.controllers;


import com.corp.abc.banking.models.Account;
import com.corp.abc.banking.repositories.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.Arrays.asList;

@RestController
@RequestMapping("/api")
public class AccountController {

    private final static Logger LOGGER = LoggerFactory.getLogger(AccountController.class);
    private final com.corp.abc.banking.repositories.AccountRepository AccountRepository;

    public AccountController(AccountRepository AccountRepository) {
        this.AccountRepository = AccountRepository;
    }

    @PostMapping("account")
    @ResponseStatus(HttpStatus.CREATED)
    public Account postAccount(@RequestBody Account account) {
        return AccountRepository.save(account);
    }


    @GetMapping("accounts")
    public List<Account> getAccounts() {
        return AccountRepository.findAll();
    }

    @GetMapping("account/{id}")
    public ResponseEntity<Account> getAccount(@PathVariable String id) {
        Account account = AccountRepository.findOne(id);
        if (account == null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        return ResponseEntity.ok(account);
    }

    @GetMapping("accounts/{ids}")
    public List<Account> getAccounts(@PathVariable String ids) {
        List<String> listIds = asList(ids.split(","));
        return AccountRepository.findAll(listIds);
    }

    @GetMapping("accounts/count")
    public Long getCount() {
        return AccountRepository.count();
    }

    @DeleteMapping("account/{id}")
    public Long deleteAccount(@PathVariable String id) {
        return AccountRepository.delete(id);
    }

    @DeleteMapping("accounts/{ids}")
    public Long deleteAccounts(@PathVariable String ids) {
        List<String> listIds = asList(ids.split(","));
        return AccountRepository.delete(listIds);
    }

    @DeleteMapping("accounts")
    public Long deleteAccounts() {
        return AccountRepository.deleteAll();
    }

    @PutMapping("account")
    public Account putAccount(@RequestBody Account account) {
        return AccountRepository.update(account);
    }


    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final Exception handleAllExceptions(RuntimeException e) {
        LOGGER.error("Internal server error.", e);
        return e;
    }
}
