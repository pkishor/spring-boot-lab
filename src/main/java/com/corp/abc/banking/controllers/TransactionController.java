package com.corp.abc.banking.controllers;


import com.corp.abc.banking.models.Account;
import com.corp.abc.banking.models.Transaction;
import com.corp.abc.banking.repositories.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.Arrays.asList;

@RestController
@RequestMapping("/api")
public class TransactionController {

    private final static Logger LOGGER = LoggerFactory.getLogger(TransactionController.class);
    private final com.corp.abc.banking.repositories.TransactionRepository TransactionRepository;

    public TransactionController(TransactionRepository TransactionRepository) {
        this.TransactionRepository = TransactionRepository;
    }

    @PostMapping("transaction")
    @ResponseStatus(HttpStatus.CREATED)
    public Transaction postTransaction(@RequestBody Transaction transaction) {
        return TransactionRepository.save(transaction);
    }

    @GetMapping("transaction/{id}")
    public ResponseEntity<Transaction> getTransaction(@PathVariable String id) {
        Transaction transaction = TransactionRepository.findOne(id);
        if (transaction == null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        return ResponseEntity.ok(transaction);
    }

    @GetMapping("transactions/{accountId}")
    public List<Transaction> getAccounts(@PathVariable String accountId) {
        return TransactionRepository.findAll(accountId);
    }

    @DeleteMapping("transaction/{id}")
    public Long deleteTransaction(@PathVariable String id) {
        return TransactionRepository.delete(id);
    }


    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final Exception handleAllExceptions(RuntimeException e) {
        LOGGER.error("Internal server error.", e);
        return e;
    }
}
