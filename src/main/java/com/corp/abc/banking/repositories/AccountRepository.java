package com.corp.abc.banking.repositories;

import com.corp.abc.banking.models.Account;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository {
    Account save(Account account);

    List<Account> findAll();

    List<Account> findAll(List<String> ids);

    Account findOne(String id);

    long count();

    long delete(String id);

    long delete(List<String> ids);

    long deleteAll();

    Account update(Account account);



}
