package com.corp.abc.banking.repositories;


import com.corp.abc.banking.models.Account;
import com.mongodb.ReadConcern;
import com.mongodb.ReadPreference;
import com.mongodb.TransactionOptions;
import com.mongodb.WriteConcern;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.WriteModel;
import org.bson.BsonDocument;
import org.bson.BsonNull;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.ReturnDocument.AFTER;
import static java.util.Arrays.asList;

@Repository
public class MongoDBAccountRepository implements AccountRepository {

    private static final TransactionOptions txnOptions = TransactionOptions.builder()
            .readPreference(ReadPreference.primary())
            .readConcern(ReadConcern.MAJORITY)
            .writeConcern(WriteConcern.MAJORITY)
            .build();
    @Autowired
    private MongoClient client;
    private MongoCollection<Account> accountsCollection;

    @PostConstruct
    void init() {
        accountsCollection = client.getDatabase("ConsumerBanking").getCollection("accounts", Account.class);
    }

    @Override
    public Account save(Account account) {
        account.setId(new ObjectId());
        account.setBalance(BigDecimal.valueOf(0));
        accountsCollection.insertOne(account);
        return account;
    }


    @Override
    public List<Account> findAll() {
        return accountsCollection.find().into(new ArrayList<>());
    }

    @Override
    public List<Account> findAll(List<String> accountIDs) {
        return accountsCollection.find(in("accountIDs", accountIDs)).into(new ArrayList<>());
    }

    @Override
    public Account findOne(String accountId) {
        return accountsCollection.find(eq("accountId", accountId)).first();
    }

    @Override
    public long count() {
        return accountsCollection.countDocuments();
    }

    @Override
    public long delete(String accountId) {
        return accountsCollection.deleteOne(eq("_id", accountId)).getDeletedCount();
    }

    @Override
    public long delete(List<String> accountIds) {
        try (ClientSession clientSession = client.startSession()) {
            return clientSession.withTransaction(
                    () -> accountsCollection.deleteMany(clientSession, in("accountId", accountIds)).getDeletedCount(),
                    txnOptions);
        }
    }

    @Override
    public long deleteAll() {
        try (ClientSession clientSession = client.startSession()) {
            return clientSession.withTransaction(
                    () -> accountsCollection.deleteMany(clientSession, new BsonDocument()).getDeletedCount(), txnOptions);
        }
    }

    @Override
    public Account update(Account account) {
        FindOneAndReplaceOptions options = new FindOneAndReplaceOptions().returnDocument(AFTER);
        return accountsCollection.findOneAndReplace(eq("accountId", account.getAccountId()), account, options);
    }


    private List<ObjectId> mapToObjectIds(List<String> ids) {
        return ids.stream().map(ObjectId::new).collect(Collectors.toList());
    }
}
