package com.corp.abc.banking.repositories;


import com.corp.abc.banking.models.Account;
import com.corp.abc.banking.models.Transaction;
import com.mongodb.ReadConcern;
import com.mongodb.ReadPreference;
import com.mongodb.TransactionOptions;
import com.mongodb.WriteConcern;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.WriteModel;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.ReturnDocument.AFTER;
import static com.mongodb.client.model.Updates.inc;

@Repository
public class MongoDBTransactionRepository implements TransactionRepository {

    private static final TransactionOptions txnOptions = TransactionOptions.builder()
            .readPreference(ReadPreference.primary())
            .readConcern(ReadConcern.MAJORITY)
            .writeConcern(WriteConcern.MAJORITY)
            .build();
    @Autowired
    private MongoClient client;
    private MongoCollection<Account> accountsCollection;
    private MongoCollection<Transaction> transactionsCollection;

    @PostConstruct
    void init() {
        accountsCollection = client.getDatabase("ConsumerBanking").getCollection("accounts", Account.class);
        transactionsCollection = client.getDatabase("ConsumerBanking").getCollection("transactions", Transaction.class);
    }

    @Override
    public Transaction save(Transaction transaction) {
        transaction.setId(new ObjectId());
        BigDecimal amount = transaction.getAmount();
        transactionsCollection.insertOne(transaction);
        if (transaction.getTransactionType().toUpperCase().equals("DEBIT")) {
            amount = transaction.getAmount().multiply(BigDecimal.valueOf(-1));
        }

        accountsCollection.updateOne(eq("accountId", transaction.getAccountId()), inc("balance", amount));

        return transaction;
    }


    @Override
    public Transaction findOne(String transactionId) {
        return transactionsCollection.find(eq("transactionId", transactionId)).first();
    }

    @Override
    public List<Transaction> findAll(String accountId) {
        return transactionsCollection.find(eq("accountId", accountId)).into(new ArrayList<>());
    }

    @Override
    public long delete(String transactionId) {
        return transactionsCollection.deleteOne(eq("_id", transactionId)).getDeletedCount();
    }

    

    @Override
    public Transaction update(Transaction transaction) {
        FindOneAndReplaceOptions options = new FindOneAndReplaceOptions().returnDocument(AFTER);
        return transactionsCollection.findOneAndReplace(eq("transactionId", transaction.getAccountId()), transaction, options);
    }

    

    private List<ObjectId> mapToObjectIds(List<String> ids) {
        return ids.stream().map(ObjectId::new).collect(Collectors.toList());
    }
}
