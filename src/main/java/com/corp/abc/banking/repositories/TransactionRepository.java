package com.corp.abc.banking.repositories;

import com.corp.abc.banking.models.Account;
import com.corp.abc.banking.models.Transaction;
import com.corp.abc.banking.models.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository {
    Transaction save(Transaction transaction);  

    Transaction findOne(String id);
    List<Transaction> findAll(String accountId);
    long delete(String id);    

    Transaction update(Transaction transaction);    

}
