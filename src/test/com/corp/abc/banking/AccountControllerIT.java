package com.corp.abc.banking;

import com.corp.abc.banking.models.Account;
import com.corp.abc.banking.repositories.AccountRepository;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AccountControllerIT {

    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate rest;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TestHelper testHelper;
    private String URL;

    @Autowired
    AccountControllerIT(MongoClient mongoClient) {
        createAccountCollectionIfNotPresent(mongoClient);
    }

    @PostConstruct
    void setUp() {
        URL = "http://localhost:" + port + "/api";
    }

    @AfterEach
    void tearDown() {
        accountRepository.deleteAll();
    }

    @DisplayName("POST /account with 1 account")
    @Test
    void postPerson() {
        // GIVEN
        // WHEN
        ResponseEntity<Account> result = rest.postForEntity(URL + "/account", testHelper.getMax(), Account.class);
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        Account accountResult = result.getBody();
        assertThat(accountResult.getId()).isNotNull();
        assertThat(accountResult).isEqualToIgnoringGivenFields(testHelper.getMax(), "id", "createdAt");
    }

    @DisplayName("POST /accounts with 2 account")
    @Test
    void postPersons() {
        // GIVEN
        // WHEN
        HttpEntity<List<Account>> body = new HttpEntity<>(testHelper.getListMaxAlex());
        ResponseEntity<List<Account>> response = rest.exchange(URL + "/accounts", HttpMethod.
                POST, body, new ParameterizedTypeReference<List<Account>>() {
        });
        // THEN
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).usingElementComparatorIgnoringFields("id", "createdAt")
                .containsExactlyInAnyOrderElementsOf(testHelper.getListMaxAlex());
    }

    @DisplayName("GET /accounts with 2 accounts")
    @Test
    void getPersons() {
        // GIVEN
        List<Account> personsInserted = accountRepository.saveAll(testHelper.getListMaxAlex());
        // WHEN
        ResponseEntity<List<Account>> result = rest.exchange(URL + "/accounts", HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Account>>() {
                });
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).containsExactlyInAnyOrderElementsOf(personsInserted);
    }

    @DisplayName("GET /account/{id}")
    @Test
    void getPersonById() {
        // GIVEN
        Account personInserted = accountRepository.save(testHelper.getMax());
        ObjectId idInserted = personInserted.getId();
        // WHEN
        ResponseEntity<Account> result = rest.getForEntity(URL + "/account/" + idInserted, Account.class);
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(personInserted);
    }

    @DisplayName("GET /accounts/{ids}")
    @Test
    void getPersonsByIds() {
        // GIVEN
        List<Account> personsInserted = accountRepository.saveAll(testHelper.getListMaxAlex());
        List<String> idsInserted = personsInserted.stream().map(Account::getId).map(ObjectId::toString).collect(toList());
        // WHEN
        String url = URL + "/accounts/" + String.join(",", idsInserted);
        ResponseEntity<List<Account>> result = rest.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Account>>() {
        });
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).containsExactlyInAnyOrderElementsOf(personsInserted);
    }

   /* @DisplayName("GET /accounts/count")
    @Test
    void getCount() {
        // GIVEN
        accountRepository.saveAll(testHelper.getListMaxAlex());
        // WHEN
        ResponseEntity<Long> result = rest.getForEntity(URL + "/accounts/count", Long.class);
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(2L);
    }

    @DisplayName("DELETE /account/{id}")
    @Test
    void deletePersonById() {
        // GIVEN
        Account personInserted = accountRepository.save(testHelper.getMax());
        ObjectId idInserted = personInserted.getId();
        // WHEN
        ResponseEntity<Long> result = rest.exchange(URL + "/account/" + idInserted.toString(), HttpMethod.DELETE, null,
                new ParameterizedTypeReference<Long>() {
                });
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(1L);
        assertThat(accountRepository.count()).isEqualTo(0L);
    }

    @DisplayName("DELETE /accounts/{ids}")
    @Test
    void deletePersonsByIds() {
        // GIVEN
        List<Account> personsInserted = accountRepository.saveAll(testHelper.getListMaxAlex());
        List<String> idsInserted = personsInserted.stream().map(Account::getId).map(ObjectId::toString).collect(toList());
        // WHEN
        ResponseEntity<Long> result = rest.exchange(URL + "/accounts/" + String.join(",", idsInserted), HttpMethod.DELETE, null,
                new ParameterizedTypeReference<Long>() {
                });
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(2L);
        assertThat(accountRepository.count()).isEqualTo(0L);
    }

    @DisplayName("DELETE /accounts")
    @Test
    void deletePersons() {
        // GIVEN
        accountRepository.saveAll(testHelper.getListMaxAlex());
        // WHEN
        ResponseEntity<Long> result = rest.exchange(URL + "/accounts", HttpMethod.DELETE, null,
                new ParameterizedTypeReference<Long>() {
                });
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(2L);
        assertThat(accountRepository.count()).isEqualTo(0L);
    }

    @DisplayName("PUT /account")
    @Test
    void putPerson() {
        // GIVEN
        Account personInserted = accountRepository.save(testHelper.getMax());
        // WHEN
        personInserted.setAge(32);
        personInserted.setInsurance(false);
        HttpEntity<Account> body = new HttpEntity<>(personInserted);
        ResponseEntity<Account> result = rest.exchange(URL + "/account", HttpMethod.PUT, body, new ParameterizedTypeReference<Account>() {
        });
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(accountRepository.findOne(personInserted.getId().toString()));
        assertThat(result.getBody().getAge()).isEqualTo(32);
        assertThat(result.getBody().getInsurance()).isFalse();
        assertThat(accountRepository.count()).isEqualTo(1L);
    }

    @DisplayName("PUT /accounts with 2 accounts")
    @Test
    void putPersons() {
        // GIVEN
        List<Account> personsInserted = accountRepository.saveAll(testHelper.getListMaxAlex());
        // WHEN
        personsInserted.get(0).setAge(32);
        personsInserted.get(0).setInsurance(false);
        personsInserted.get(1).setAge(28);
        personsInserted.get(1).setInsurance(true);
        HttpEntity<List<Account>> body = new HttpEntity<>(personsInserted);
        ResponseEntity<Long> result = rest.exchange(URL + "/accounts", HttpMethod.PUT, body, new ParameterizedTypeReference<Long>() {
        });
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(2L);
        Account max = accountRepository.findOne(personsInserted.get(0).getId().toString());
        Account alex = accountRepository.findOne(personsInserted.get(1).getId().toString());
        assertThat(max.getAge()).isEqualTo(32);
        assertThat(max.getInsurance()).isFalse();
        assertThat(alex.getAge()).isEqualTo(28);
        assertThat(alex.getInsurance()).isTrue();
        assertThat(accountRepository.count()).isEqualTo(2L);
    }

    @DisplayName("GET /accounts/averageAge")
    @Test
    void getAverageAge() {
        // GIVEN
        accountRepository.saveAll(testHelper.getListMaxAlex());
        // WHEN
        ResponseEntity<Long> result = rest.getForEntity(URL + "/accounts/averageAge", Long.class);
        // THEN
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isEqualTo(29L);
    }
*/
    private void createAccountCollectionIfNotPresent(MongoClient mongoClient) {
        // This is required because it is not possible to create a new collection within a multi-documents transaction.
        // Some tests start by inserting 2 documents with a transaction.
        MongoDatabase db = mongoClient.getDatabase("test");
        if (!db.listCollectionNames().into(new ArrayList<>()).contains("accounts"))
            db.createCollection("accounts");
    }
}
