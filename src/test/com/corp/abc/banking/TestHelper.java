package com.corp.abc.banking;

import com.corp.abc.banking.models.Account;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Arrays.asList;

@Component
class TestHelper {

    Account getMax() {
        return new Account();
    }



    List<Account> getListMaxAlex() {
        return asList(getMax(), getMax());
    }
}
